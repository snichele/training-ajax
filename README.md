# Formation Ajax

## Présentation des sources

Ce repository contient les énoncés / solutions des TPs de la formation Ajax.

La branche *master* contient les points de départ des exercices ; la branche *solution* les solutions.

## Utilisation

- Récupérez la branche du repository qui vous intéresse
- Récupérez, sous l'onglet *Downloads* le fichier nodejs-XXXXXXX.zip

    - Ce fichier zip contient un serveur http codé en javascript, node.js
    - Ce serveur doit être dézippé dans le répertoire contenant les sources du présent répository, au même niveau que le répertoire *public-html*
    - Une fois le fichier dézippé dans un répertoire du même nom, lancez le serveur node.js en exécutant le fichier *runAjaxServer.cmd*

- Travaillez dans le répertoire /public_html/tpXX, où XX représente le TP en cours.
- Pour tester le code écrit, lancez un navigateur web pointant sur l'adresse http://127.0.0.1:8080/tpXX/xxxx.html , en remplaçant les xxx
par les bonnes valeurs en fonction du tp que vous voulez tester.
    
    - Par exemple : [http://127.0.0.1:8080/tp1/tp_xhr.html](http://127.0.0.1:8080/tp1/tp_xhr.html)


- **Ne testez pas les requêtes Ajax en ouvrant vos fichiers html directement dans le navigateur, les
requêtes ne peuvent pas être executées en dehors d'un serveur web**

## Enoncés des TPS

### TP 1 – Requête classique AJAX et remise à jour d’un élément de la page.


Vous allez commencer par effectuer une requête ajax classique (avec l’objet XMLHttpRequest) pour remettre à jour le compteur de tweets.

1.	Modifiez le bouton « tweet » pour que quand on clique dessus une fonction javascript soit exécutée
2.	Cette fonction aura pour rôle de 

    1.	faire une requête ajax vers l’url **/tp1/tweet** (url spéciale configurée dans le serveur node.js, ne cherchez pas le fichier correspondant) 
    2.	récupérer le résultat
    3.	mettre à jour le compteur de tweet (en haut à gauche) avec la valeur retournée

3.	Pour ce faire, vous devrez :

    1.	Utiliser l’objet XMLHttpRequest
    2.	Définir la fonction de callback à exécuter quand le résultat est chargé ET que le statut de retour est OK
    3.	Faire que cette fonction callback remette à jour l’élément « nbTweets »

4.	Faire en sorte que ce code fonctionne sous IE et Firefox

    1.	Tester le navigateur et utiliser l’objet XMLHttpRequest pour Firefox, et ActiveXObject pour IE.

Vous pouvez utiliser l’url suivant comme référence pour l’objet XMLHttpRequest :
http://www.xul.fr/Objet-XMLHttpRequest.html

### TP 2 – Manipulation du DOM avec le résultat d’une requête Ajax

On reprend l’exemple précédent. Désormais, quand on clique sur le bouton tweet, il faut que le contenu du tweet soit envoyé au serveur dans une requête GET. (la page serveur à attaquer est **/tp2/tweet**, et le contenu du tweet doit passer en tant que paramètre GET de nom *tweet*).

La page **/tp2/tweet** renverra simplement le contenu du tweet. Il faudra alors récupérer ce résultat et créer un rectangle (élément div) affichant le tweet (classe ‘post’) sur le modèle du TP précédent.

Vous pouvez créer  cet élément div de 2 manières différentes :

-	En ajoutant un bout de html brut au div d’id ‘posts’ (conteneur des div de type ‘post’)
-	En créant un div que vous ajouterez en tant que neoud fils de l’élément ‘posts’ (utliser les fonctions document.getElementById(), document.createElement() et appendChild())

N’oubliez pas de reproduire la structure du div ‘post’ avec le contenu du tweet et le div timestamp.

Par ailleurs vous devrez générer un identifiant unique pour le div que vous ajoutez au document (utilisez la fonction « new Date().getTime() » pour vous aider à créer un identifiant unique).


### TP 3 – Requête ajax en utilisant jquery

Refaites le TP 2 en utilisant la fonction ajax() de jquery.
La page serveur à attaquer est **/tp3/tweet**.

- Téléchargez jquery (utilisez google pour retrouver le site)
- Importez jquery dans la page

Vous déclencherez l’appel ajax lorsque le bouton submitTweet sera cliqué (utiliser la fonction on() de jquery).

Vous remplirez l’attribut *data* de la requête ajax en serializant le contenu du formulaire theForm de la manière suivante : « data: form.serialize() »

De même vous remplirez les arguments *url* et *type* de l’appel ajax, avec les attributs *action* et *method* du formulaire theForm (utilisez la fonction attr() pour les récupérer).
Vous pouvez reprendre le cour qui présente une solution approchante.

Pour traiter le retour de la requête Ajax qui renvoit un fragment html contenant le tweet posté mis en forme, ajoutez 
simplement son contenu à l’element ‘posts’.(vous pouvez utiliser l’attribut innerHTML)

### TP 4 – Manipulation du dom avec jquery

Sur la base du TP3, vous allez enrichir les actions exécutées au moment du retour de la requête ajax.
La page serveur à attaquer est **/tp3/tweet** (idem que tp3).

Objectif :

 - le post récupéré s’ajoute en bas de la liste des post; sa couleur doit-être différente des autres posts, ainsi que son style (italique)
 - le post qui avait été soumis juste doit reprendre un style normal
 - le plus ancien post disparait

Vous devrez donc à l’aide de jquery :

-	Rajouter le html retourné par le serveur au div ‘posts’ (idem tp3)
-	Supprimer le premier post de la page (le plus haut)
-	Changer la couleur de fond du dernier post (celui que vous venez d’ajouter). Vous pouvez utiliser la couleur #FFBFD9.
-	Mettre le dernier post en italique
-	Mettre la couleur de fond de l’avant-dernier post à ‘blanc’, et lui redonner une casse normale (non italique).

### TP 5 – Récupération et utilisation d’un contenu json

Sur la base du TP4, vous ne recevez maintenant plus un morceau de html de la part du serveur, mais directement une structure json, de la forme suivante :

La page serveur à attaquer est **/tp5/tweet**

- counter
- post
    - author
    - text
    - timestamp
    - timeId

Vous devrez récupérer ce résultat et :

-	Mettre le compteur du nombre de tweets à jour
-	Construire le div du post qui contiendra :

    - Un id construit à partir de ‘timeId’
    - Le contenu du tweet
    - Le nom de l’auteur
    - L’heure et la date à partir de ‘timestamp’

Note : vous pouvez observer la structure que doit avoir votre élément post en inspectant les autres éléments post de la page à partir de firebug (ou équivalent).

### TP 6 – Interception d’événements DOM avec jquery

Le but de cet exercice est d’afficher les infos complémentaires d’un post quand on le survole.

1ere Etape :

Afficher un message d’avertissement (avec alert() ) quand la souris passe sur un post, et quand elle quitte la zone du post.

Pour cela, utilisez la fonction ‘bind’ de jquery avec les évènements ‘mouseenter’ et ‘mouseleave’.

2eme Etape :

Quand la souris passe sur un post, lancez une requête ajax vers le fichier tp_evt_hover.php. Récupérer le résultat de la requête, insérez le dans un span de classe ‘views’ que vous ajouterez à la suite du div sur lequel la souris vient d’entrer. 

Quand la souris sort du post, supprimer le span.
