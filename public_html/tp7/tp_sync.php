<?php

sleep(2);

// gestion du conteur
session_start();

if(isset($_SESSION['nbTweets'])){
	$_SESSION['nbTweets']++;
} else{
	$_SESSION['nbTweets'] = 1 ;
}

// retour du json
echo('
{
    "counter": "'.$_SESSION['nbTweets'].'",
    "post":
    {
    	"author": "'.$_POST['author'].'",
    	"text": "'.$_POST['tweet'].'",
    	"timestamp": "'.date('H:i:s, d/m/Y').'",
    	"timeId": "'.time().'"
    }
}');

?>