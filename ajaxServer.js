/*
 Attention : La variable 'webroot' pointe par défaut sur l'application orlando finale
 disponible dans l'arborescence du formateur.
 
 Il faut la modifier pour pointer sur le bon répertoire local contenant à minima un fichier index.htm.
 
 Attention II : Code à revoir, ne pas l'utiliser comme exemple de bonne pratiques !
 */

var url = require('url');
var request = require('request');
var nstatic = require('node-static');
var http = require('http');
var util = require('util');
var qs = require('querystring');
var webroot = '../public_html/', port = 8080;
var file = new (nstatic.Server)(webroot, {
    cache: 600,
    headers: {
        'X-Powered-By': 'node-static'
    }
});

http.createServer(function(req, res) {
    "use strict";
    var parsed, path, postBody = "";
    req.addListener('data', function(data) {
        console.info("data is " + data);
        postBody += data;
    });
    req.addListener('end', function() {
        if (req.method === 'GET') {
            parsed = url.parse(req.url, true);
            path = parsed.pathname;
            // Appel de l'api (non officielle) imdb
            if (path.indexOf("/tp1/tweet") == 0) {
                res.writeHead(200, {
                    'content-type': 'text/html'
                });
                res.write("" + Math.random());
                res.end('\n');
            } else
            if (path.indexOf("/tp2/tweet") == 0) {
                res.writeHead(200, {
                    'content-type': 'text/html'
                });
                var query = parsed.query;
                try {
                    res.write(query['tweet']);
                } catch (e) {
                    res.write("");
                }
                res.end('\n');
            } else if (path.indexOf("/spectacles/list") == 0) {
                res.writeHead(200, {
                    'content-type': 'text/plain'
                });
                res.write('{titre : "The Godfather",duree : 123,dts : false}');
                res.end('\n');
                // Fichiers statiques
            } else {
                file.serve(req, res, function(err, result) {
                    if (err) {
                        console.error('Error serving %s - %s', req.url, err.message);
                        res.writeHead(err.status, err.headers);
                        res.end();
                    } else {
                        console.log('%s - %s', req.url, res.message);
                    }
                });
            }
        } else if (req.method === 'POST') {
            parsed = url.parse(req.url, true);
            path = parsed.pathname;
            var POST = qs.parse(postBody);
            // use POST
            console.info(POST);

            if (path.indexOf("/tp3/tweet") === 0) {
                res.writeHead(200, {
                    'content-type': 'text/plain'
                });
                res.write('<div id="' + Math.random() + '" class="post" >' + POST['tweet'] + '<div class="author" >by ' + POST['author'] + '</div><div class="timestamp" >' + new Date() + '</div></div>');
                res.end('\n');
            } else
            if (path.indexOf("/tp5/tweet") === 0) {
                res.writeHead(200, {
                    'content-type': 'text/plain'
                });
                res.write(
                        '{ "counter": "'
                        + Math.random()
                        + '","post": {"author": "'
                        + POST['author']
                        + '", "text": "'
                        + POST['tweet']
                        + '", "timestamp": "'
                        + new Date()
                        + '","timeId": "'
                        + Math.random()
                        + '"    }}');
                res.end('\n');
            }
        }

    });
}).listen(port);
console.log(' ** Ajax training server started (running on portable node.js, version 0.6.18) **\n');
console.log(' - Running at http://localhost:%d', port);
console.log(' - Webroot is : %s', webroot);
console.log(' \n  To see your application running, open a browser at the adress http://localhost:%d/public_html/tpXX/xxx.html', port);
console.log(' \n  If something goes wrong, check that webroot is pointing to your local static application root directory and that you have an index.htm file in it.');